

import React, {useState} from 'react';
import {
  SafeAreaView,
} from 'react-native';
import InputRead from './components/InputRead';
import List from './components/List';

const App = () => {
  const [input, setInput]= useState('');
  const [names, setNames] = useState([]);
  
  const nameadd = ()=>{
    setNames([...names,input])
  }

  return (
    <SafeAreaView>
     <InputRead input={input} setInput={setInput} click={nameadd}/>
     <List name={names}/>
    </SafeAreaView>
  );
};


export default App;
