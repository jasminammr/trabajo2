import React from 'react';
import {ScrollView,Text, View} from 'react-native';
import Item from './Item';

function List({name}) {

  return (
    <ScrollView>
        {name.map(name =>
            <Item name={name}/>
        )}
    </ScrollView>
  );
}

export default List;