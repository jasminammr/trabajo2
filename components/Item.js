import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Item = (props) => {

  return (
    <View style={styles.item}>
      <Text>
          {props.name}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    width: '100%',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  
});

export default Item;
