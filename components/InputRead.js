import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';
function InputRead(props) {
    
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        value={props.input}
        onChangeText={(text) => props.setInput(text)}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={props.click}>
        <Text>Agregar</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    width: '80%',
    borderRadius: 15,
    margin: 5,
  },
  button: {
    justifyContent: 'center',
  },
});

export default InputRead;